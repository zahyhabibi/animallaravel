<!DOCTYPE html>
<html>

<head>
    <title>Trposianimal</title>
</head>

<link rel="stylesheet" type="text/css" href="public/design.css">
<link rel="stylesheet" type="text/css" href="public/css/bootstrap.css">
<link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

<body>
    <div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-transparent">
            <label class=" mx-5 h2 text-white" href="#">Troposianimal</label>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto mx-5 h5">
                    <li class="nav-item mx-4">
                        <a class="nav-link" href="{{url('/home')}}">Home<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item mx-4">
                        <a class="nav-link" href="{{url('/about')}}">Tentang<span class="sr-only">(current)</span></a>
                    </li>

                    <li class="nav-item mx-4">
                        <a class="nav-link" href="{{url('/berita')}}">Berita</a>
                    </li>
                    <li class="nav-item mx-4">
                        <a class="nav-link" href="{{url('/galery')}}">Gallery</a>
                    </li>
                    <li class="nav-item mx-4">
                        <a class="nav-link" href="{{url('/contact')}}">Kontak</a>
                    </li>
                </ul>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Home</a>
                    <a class="dropdown-item" href="#">Tentang</a>
                    <a class="dropdown-item" href="#">Berita</a>
                    <a class="dropdown-item" href="#">Gallery</a>
                    <a class="dropdown-item" href="#">Kontak</a>
                </div>
            </div>
        </nav>
    </div>
    <div class="background1">
        <header class="masthead">
            <div class="container pt-5 text-white  masthead-content">
                <h1 class="mb-4 font-weight-bold h1">Tentang Kami</h1>
                <div class="row align-items-content justify-content-center my-5">
                </div>
            </div>
        </header>
    </div>
    <div class="jumbotron">
        <div class="container pt-5 text-black masthead-content">
            <img src="public/img/dawn-armfield-84n7c9cLEKM-unsplash	.png" class="rounded float-right @media">
            <img src="public/img/kyaw-tun-_YIX48_4hgs-unsplash.png" class=" mx-4 rounded float-right @media">
            <img src="public/img/smit-patel-dGMcpbzcq1I-unsplas.png" class="rounded float-right my-4 singa @media">
            <br>
            <h1 class="font-weight-bold h1 @media">Tropisanimal</h1>
            <p class="@media">Lorem ipsum dolor sit amet, consectetur adipisicing <br> elit, sed do eiusmod
                tempor incididunt ut labore et <br> dolore magna aliqua. Ut enim ad minim veniam,
                quis<br></p>
            <p class="my-4 @media">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

            <br>
            <label class="h1 font-weight-bold my-4 mt-3"> VISI </label>
            @foreach ($profile as $row)
            <section>
                <ul class="list-unstyled">
                    <li>{{$row->Visi}}</li>
                </ul>

            </section>


            @endforeach
            <label class="h1 font-weight-bold my-4 mt-3"> MISI </label>
            @foreach ($profile as $row)
            <section>
                <ul class="list-unstyled">
                    <li>{{$row->Misi}}</li>
                </ul>

            </section>

            @endforeach

        </div>

    </div>




    <section>


        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
        </script> <br>

    </section>
    <footer class="page-footer font-small bg-dark text-white">
        <div class="container text-center text-md-left">
            <section class="ketebalan row">
                <div class="col-md-3">
                    <ul class="list-unstyled">
                        <li>
                            <h5 class="font-weight-bold text-uppercase mt-3 mb-4"><label
                                    class="ml-2">Troposianimal</label></h5>
                        </li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                            <a href="https://facebook.com" class="ml-2"><img src="public/img/001-facebook.png"></a>
                            <a href="https://twitter.com" class="ml-2"><img src="public/img/002-twitter.png"></a></li>



                    </ul>

                </div>
                <hr class="clearfix w-50 d-md-none">

                <!-- Footer 1 -->
                <div class="mx-auto">
                    <h5 class="font-weight-bold text-lowercase mt-3 mb-4"><label class="ml-2">Useful link</label></h5>
                    <ul class="list-unstyled">
                        <li>
                            <label>Blog</label>
                        </li>
                        <li>
                            <label>Hewan</label>
                        </li>
                        <li>
                            <label>Gallery</label>
                        </li>
                        <li><label>Testimonial</label></li>
                    </ul>
                </div>
                <!-- Footer 1 -->

                <!-- Footer 2 -->
                <div class="mx-auto">
                    <h5 class="font-weight-bold text-lowercase mt-3 mb-4"><label class="ml-2">Privacy</label></h5>
                    <ul class="list-unstyled">
                        <li>
                            <label>Karir</label>
                        </li>
                        <li>
                            <label>tentang Kami</label>
                        </li>
                        <li>
                            <label>Kontak Kami</label>
                        </li>
                        <li><label>Servis</label></li>
                    </ul>
                </div>
                <!-- Footer 2 -->

                <!-- Footer 3 -->
                <div class="mx-auto">
                    <h5 class="font-weight-bold text-lowercase mt-3 mb-4"><label class="ml-2 mb-3">Contact Info</label>
                    </h5>
                    <ul class="list-unstyled">
                        <li>
                            <a href="{{url('/home')}}"><img src="public/img/mail.svg"><label
                                    class="ml-2 mb-3">troposianimal@gmail.com</label></a>
                        </li>
                        <li>
                            <img src="public/img/phone.svg"><label class="ml-2 mb-3">08123456789</label>
                        </li>
                        <li>
                            <a href="#"><img src="public/img/map-pin.svg"><label class="ml-2 mb-3">Kota Bandung, Jawa
                                    Barat</label></a>
                        </li>
                    </ul>
                </div>
                <!-- Footer 3 -->
            </section>

        </div>
        <div class="footer-copyright text-center py-3"> 2020 Copyright</div>
    </footer>

    </div>
</body>

</html>
