<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NormalController extends Controller
{
     public function index(){
        return view('template');
    }

     public function form(){
    	$data['profile'] = \DB::table('t__animal')->get();
    	return view('profile/form',$data);
    }
    public function formpic(){
        $data['formv'] =\DB::table('t__picture')->get();
        return view('news/formpic',$data);
    }
    public function repairweb(){
        return view('Repair/Repair');
    }
}
