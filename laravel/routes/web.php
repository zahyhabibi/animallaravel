<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\BeritaController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\GaleryController;
// use App\http\Controllers\TemplatekuController;
// use App\http\Controllers\FormpController;
use App\http\controllers\AddController;
use App\http\controllers\NormalController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', function () {
    return view('welcome');
});

Route::get('/maintenance',[NormalController::class,'repairweb']);

//Single Controller
Route::get('/',[HomeController::class,'home']);
Route::get('/home',[HomeController::class,'home']);
Route::get('/about', [AboutController::class,'about']);
Route::get('/berita', [BeritaController::class,'news']);
Route::get('/contact', [ContactController::class,'contact']);
Route::get('/galery',[GaleryController::class,'gallery']);

//Single Controller

//Controller
Route::get('/template', [NormalController::class,'index']);
Route::get('/form', [NormalController::class,'form']);
Route::get('/formpic', [NormalController::class,'formpic']);



//main Controller 1
Route::get('/add', [AddController::class,'create']);
Route::post('/add', [AddController::class,'store']);
Route::get('/edit{id}', [AddController::class,'edit']);
Route::patch('/add/{id}',[AddController::class,'update']);
	  
// main controller 2
Route::get('/addpic',[GaleryController::class,'create']);
Route::post('/addpic',[GaleryController::class,'store']);
Route::delete('/formpic/{nopic}/delete',[GaleryController::class,'destroy']);
Route::get('/change{nopic}',[GaleryController::class,'edit']);
Route::patch('/change/{nopic}',[GaleryController::class,'update']);

//main controller 3
Route::get('/news',[BeritaController::class,'news']);