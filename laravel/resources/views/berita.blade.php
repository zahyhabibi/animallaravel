<!DOCTYPE html>
<html>

<head>
    <title></title>
</head>
<link rel="stylesheet" type="text/css" href="public/design.css">
<link rel="stylesheet" type="text/css" href="public/css/bootstrap.css">
<link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

<body class="poppins" id="font">
    <div class="background1">
        <nav class="navbar navbar-expand-lg navbar-dark bg-transparent">
            <label class=" mx-5 h2 text-white" href="#">Troposianimal</label>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto mx-5 h5">
                    <li class="nav-item mx-4">
                        <a class="nav-link" href="{{url('/home')}}">Home<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item mx-4">
                        <a class="nav-link" href="{{url('/about')}}">Tentang</a>
                    </li>
                    <li class="nav-item mx-4">
                        <a class="nav-link" href="{{url('/berita')}}">Berita</a>
                    </li>
                    <li class="nav-item mx-4">
                        <a class="nav-link" href="{{url('/galery')}}">Gallery</a>
                    </li>
                    <li class="nav-item mx-4">
                        <a class="nav-link" href="{{url('/contact')}}">Kontak</a>
                    </li>
                </ul>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Home</a>
                    <a class="dropdown-item" href="#">Tentang</a>
                    <a class="dropdown-item" href="#">Berita</a>
                    <a class="dropdown-item" href="#">Gallery</a>
                    <a class="dropdown-item" href="#">Kontak</a>
                </div>
            </div>
        </nav>
        <div class="background1">
            <header class="masthead">
                <div class="container pt-5 text-white  masthead-content">
                    <h1 class="mb-4 font-weight-bold h1">Berita</h1>
                    <div class="row align-items-content justify-content-center my-5">
                    </div>
                </div>
            </header>
        </div>
        </header>

        <section>



            <div class="jumbotron">
                <div class="container">
                    <div>
                        <h2 class="font-weight-bold h2 rounded float-right mx-3">
                            10 Hewan Herbivora<br> Paling Berbahaya
                        </h2>
                        <p class="rounded float-right textposisi my-0">Lorem ipsum dolor sit amet, consectetur
                            adipisicing<br> elit, sed do eiusmod
                            tempor incididunt ut labore et <br> dolore magna aliqua. Ut enim ad minim veniam,
                            quis<br><br>
                            lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do <br> eiusmod
                            tempor incididunt ut labore et dolore magna aliqua.<br> Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco<br> laboris nisi ut aliquip ex ea commodo
                            consequat.<br>
                            <a href="{{'maintenance'}}" class="btn btn-success btn-lg @media rounded float-left" role="button"
                                role="button">Baca Selengkapnya
                                <img src="public/img/arrow-right.png"></a>
                    </div>

                    <img src="public/img/zoe-reeve-9hSejnboeTY-unsplash.png">
                    <img class="kudanil" src="public/img/david-clode-AtCChdVhAmA-unsplash.png">
                    <img class="badak" src="public/img/ronald-gijezen-7h06P9UKhYY-unsplash.png">
                    <div class="container pt-5 text-white  masthead-content mx-4">




                        <div id="news">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 thumb">
                                        <div class="thumbnail text-black text-center bg-light">
                                            <img class="mt-0 w-100 h-75 px-1 py-1"
                                                src="public/img/rick-l-037fCBgZB10-unsplash.png">
                                            <div class="caption">
                                                <a href="{{'maintenance'}}">
                                                    <p class="text-center h5 font-weight-bold text-black-50"><br>Apa
                                                        kabar kebun binatang<br>saat wabah COVID-19?<br></p>
                                                    <br>
                                                    <p class="text-black-50">Lorem ipsum dolor sit amet, consectetur
                                                        adipisicing elit, sed do eiusmod
                                                        tempor incididunt</p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 thumb">
                                        <div class="thumbnail text-black text-center bg-light">
                                            <img class=" mt-0 w-100 h-100 px-1 py-1"
                                                src="public/img/hans-veth-o33FMDaXJS8-unsplash.png">
                                            <div class="caption">
                                                <a href="{{'maintenance'}}">
                                                    <p class="text-center h5 font-weight-bold text-black-50"><br>Anugrah
                                                        dari hutan<br>INDONESIA<br></p>
                                                    <br>
                                                    <p class="text-black-50">Lorem ipsum dolor sit amet, consectetur
                                                        adipisicing elit, sed do eiusmod
                                                        tempor incididunt</p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 thumb">
                                        <div class="thumbnail text-black text-center bg-light">
                                            <img class=" mt-0 w-100 h-100 px-1 py-1"
                                                src="public/img/ronald-gijezen-7h06P9UKhYY-unsplash.png">
                                            <div class="caption">
                                                <a href="{{'maintenance'}}">
                                                    <p class="text-center h5 font-weight-bold text-black-50"><br>10
                                                        Hewan Herbivora<br>Yang berbahaya<br></p>
                                                    <br>
                                                    <p class="text-black-50">Lorem ipsum dolor sit amet, consectetur
                                                        adipisicing elit, sed do eiusmod
                                                        tempor incididunt</p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 thumb">
                                        <div class="thumbnail text-black text-center bg-light">
                                            <img class="mt-0 w-100 h-100 mw-100 mh-100 px-1 py-1"
                                                src="public/img/smit-patel-dGMcpbzcq1I-unsplas.png">
                                            <div class="caption">
                                                <a href="{{'maintenance'}}">
                                                    <p class="text-center h5 font-weight-bold text-black-50"><br>4
                                                        Penyakit yang ditularkan<br>hewan ke manusia<br></p>
                                                    <br>
                                                    <p class="text-black-50">Lorem ipsum dolor sit amet, consectetur
                                                        adipisicing elit, sed do eiusmod
                                                        tempor incididunt</p>
                                            </div>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 thumb">
                                        <div class="thumbnail text-black text-center bg-light">
                                            <img class=" mt-0 w-100 h-100 px-1 py-1" src="public/img/TERUMBU-KARANG (1).png">
                                            <div class="caption">
                                                <a href="{{'maintenance'}}">
                                                    <p class="text-center h5 font-weight-bold text-black-50"><br>Terumbu
                                                        karang: Pengertian,<br> jenis, seraban dan masalah<br></p>
                                                    <br>
                                                    <p class="text-black-50">Lorem ipsum dolor sit amet, consectetur
                                                        adipisicing elit, sed do eiusmod
                                                        tempor incididunt</p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 thumb">
                                        <div class="thumbnail text-black text-center bg-light">
                                            <img class=" mt-0 w-100 h-100 px-1 py-1"
                                                src="public/img/vladimir-kudinov-vmlJcey6HEU-unsplash.png">
                                            <div class="caption">
                                                <a href="{{'maintenance'}}">
                                                    <p class="text-center h5 font-weight-bold text-black-50">
                                                        <br>Ternyata tanduk rusa berasal<br>dari sel kangker tulang<br>
                                                    </p>
                                                    <br>
                                                    <p class="text-black-50">Lorem ipsum dolor sit amet, consectetur
                                                        adipisicing elit, sed do eiusmod
                                                        tempor incididunt</p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                        crossorigin="anonymous"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
                        crossorigin="anonymous"></script>
                    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
                        crossorigin="anonymous"></script> <br>

        </section>
        <footer class="page-footer font-small bg-dark text-white">
        <div class="container text-center text-md-left">
            <section class="ketebalan row">
                <div class="col-md-3">
                    <ul class="list-unstyled">
                        <li>
                            <h5 class="font-weight-bold text-uppercase mt-3 mb-4"><label
                                    class="ml-2">Troposianimal</label></h5>
                        </li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                            <a href="https://facebook.com" class="ml-2"><img src="public/img/001-facebook.png"></a>
                            <a href="https://twitter.com" class="ml-2"><img src="public/img/002-twitter.png"></a></li>



                    </ul>

                </div>
                <hr class="clearfix w-50 d-md-none">

                <!-- Footer 1 -->
                <div class="mx-auto">
                    <h5 class="font-weight-bold text-lowercase mt-3 mb-4"><label class="ml-2">Useful link</label></h5>
                    <ul class="list-unstyled">
                        <li>
                            <label>Blog</label>
                        </li>
                        <li>
                            <label>Hewan</label>
                        </li>
                        <li>
                            <label>Gallery</label>
                        </li>
                        <li><label>Testimonial</label></li>
                    </ul>
                </div>
                <!-- Footer 1 -->

                <!-- Footer 2 -->
                <div class="mx-auto">
                    <h5 class="font-weight-bold text-lowercase mt-3 mb-4"><label class="ml-2">Privacy</label></h5>
                    <ul class="list-unstyled">
                        <li>
                            <label>Karir</label>
                        </li>
                        <li>
                            <label>tentang Kami</label>
                        </li>
                        <li>
                            <label>Kontak Kami</label>
                        </li>
                        <li><label>Servis</label></li>
                    </ul>
                </div>
                <!-- Footer 2 -->

                <!-- Footer 3 -->
                <div class="mx-auto">
                    <h5 class="font-weight-bold text-lowercase mt-3 mb-4"><label class="ml-2 mb-3">Contact Info</label>
                    </h5>
                    <ul class="list-unstyled">
                        <li>
                            <a href="{{url('/home')}}"><img src="public/img/mail.svg"><label
                                    class="ml-2 mb-3">troposianimal@gmail.com</label></a>
                        </li>
                        <li>
                            <img src="public/img/phone.svg"><label class="ml-2 mb-3">08123456789</label>
                        </li>
                        <li>
                            <a href="#"><img src="public/img/map-pin.svg"><label class="ml-2 mb-3">Kota Bandung, Jawa
                                    Barat</label></a>
                        </li>
                    </ul>
                </div>
                <!-- Footer 3 -->
            </section>

        </div>
        <div class="footer-copyright text-center py-3"> 2020 Copyright</div>
    </footer>
    </div>
    </div>
</body>

</html>
