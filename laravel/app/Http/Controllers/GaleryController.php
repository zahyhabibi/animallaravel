<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GaleryController extends Controller
{
    public function gallery()
    {
        $data['formv'] =\DB::table('t__picture')->get();
        return view('galery',$data);
//fungsi create data
}
    public function create(){
        return view('news/addpic');
    }
    public function store(Request $request){

        $input = $request->all();
        unset($input ['_token']);
        
        if($request->hasFile('apic') && $request->file('apic')->isValid()){
            $filename = $request->file('apic')->getClientOriginalName();
            $request->file('apic')->storeAs('img', $filename);
            $input['apic'] =  $filename;
        }

        $status = \DB::table('t__picture')->insert($input);

        if($status){
            return redirect('formpic')->with('success','Add Picture Success');
        }else{
            return redirect('news/addpic')->with('error','Add Picture Failed');
        }
        
    }
    public function destroy(Request $request, $id){
        $result = \DB::table('t__picture')->where('nopic', $id);
        $status = $result->delete();

        if($status) return redirect('formpic')->with('success','Add Picture Success');

    }
    
    public function edit($id)
    {
        $data['editpic'] =\DB::table('t__picture')->where('nopic','=',$id)->first();
        return view('news/addpic',$data);
    }
    public function update(Request $request, $id){
       
        $input = $request->all();
        unset($input ['_token']);
        unset($input['_method']);
        
        if($request->hasFile('apic') && $request->file('apic')->isValid()){
            $filename = $request->file('apic')->getClientOriginalName();
            $request->file('apic')->storeAs('img', $filename);
            $input['apic'] =  $filename;
        }
        $status = \DB::table('t__picture')->where('nopic',$id)->update($input);
        if($status){
            return redirect('/formpic')->with('success','Edit Picture success');
        }else{
            return redirect('news/addpic')->with('error','Edit Picture Failed');
        }
    
    }
    
} 