<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AddController extends Controller
{
 //   public function index()
 //   {

 //   	$data['add'] = \DB::table('t__animal')
 //   	->get();
 //   	return view('profile/add',$data);
 // }
public function create(){
    return view('profile/add');
}

    
    public function store (Request $request){
    	$input = $request->all();
    	unset ($input['_token']);
    	$status = \DB::table('t__animal')->insert($input);
    	if($status) {
    		return redirect('/form')->with('success','Data Added Success');
    	}else{
    		return redirect('/profile/add')->with('error','Data Added Failed');
    	}
	}
	// Tambah Data

    public function edit($id)
    {
    	$data['add'] = \DB::table('t__animal')
        ->find($id);
    	return view('profile/add', $data);
    }

    public function update(Request $request, $id)
    {
    	$rule = [
    		'Visi' => 'required',
    		'Misi' => 'required'
    	];

    	$this->validate($request, $rule);
    	$input = $request->all();
    	unset($input ['_token']);
    	unset($input ['_method']);

    	$status =\DB::table('t__animal')->where('id',$id)->update($input);
    	if($status){
    		return redirect('/form')->with('success','Data Update Success');
    	}else{
    		return redirect('/form/add')->with('error','Data Update Failed');
    	}
    }

}