<?php

namespace App\Models;

use illuminate\Database\Eloquent\Model;

class gallery extends Model{
    public $primarykey = 'nopic';

    protected $table ='t__picture';

    protected $fillable=['apic'];
}